/*global $ */
//========================Navbar=====================//
$(document).ready(function (){
   
  $(".navbar li a").click(function (e){
      
       e.preventDefault();
        $("html, body").animate({
            scrollTop: $("#"+ $(this).data("scroll")).offset().top - 70
        }, 1000);
      //Add Class Active
      $(this).addClass("active").parent().siblings().find("a").removeClass("active");
   });
    // Sycn Navbar
    
    $(window).scroll(function (){
        $("section").each(function (){
            if($(window).scrollTop() > ($(this).offset().top - 100)){
                sectionID=$(this).attr("id");
                $(".navbar li a ").removeClass("active");
                $(".navbar li a[data-scroll='"+ sectionID +"']").addClass("active");
            }
        })
    })
});
//========================Scroll To Top=====================//
$(document).ready(function (){

    var scrollButton=$("#scroll");

    $(window).scroll(function(){
        if($(this).scrollTop()>=700)
        {
            scrollButton.show();
        }
        else
        {
            scrollButton.hide();
        }
    });
    scrollButton.click(function(){
        $("html,body").animate({scrollTop:0},900);
    });
});
$(document).ready(function(){
  var owl = $('.home-slider .owl-carousel');
    owl.owlCarousel({
        loop:false,
        nav:true,
        dots:false,
        margin:10,
        items:1,
        rtl:true,
        autoplay:true,
        autoplayTimeout:8000,
        smartSpeed:550,
        autoplayHoverPause:true,
        responsiveClass:true,
//        animateOut: 'fadeOut',
//        animateIn: 'fadeIn'
            
    });
    
});
//========================Load=====================//
$(document).ready(function () {
    "use strict";
    $(window).load(function () {
        $(".load").fadeOut(100, function () {
            $("body").css("overflow", "auto");
            $(this).fadeOut(100, function () {
                $(this).remove();
            });
        });
    });

});
//========================WOW=====================//
// Init WOW.js and get instance
    var wow = new WOW();
    wow.init();

